# Customization of OpenIPMC-FW for the Pulsar-2b ATCA carrier

Following the customizable approach of OpenIPMC-FW, the OpenIPMC-SW software is included in this FW as a *submodule*. This keeps OpenIPMC-SW - which provides generic resources and is maintained separately by the OpenIPMC central team - separated from this specific implementation for the Pulsar-2b, while the needed board-specific customizations are kept in this project.


## Makefiles

The build recipes for the ARM Cortex-M7 firmware are in `openipmc-fw/CM7/Makefile`. However, in order to link the board-specific code, it includes `board_specific.mk` which must be present in this project defining the include paths and sources files. A very generic example of `board_specific.mk` is provided here. Note that all paths need to be relative to `openipmc-fw/CM7`.

This project also have a main `Makefile` which simplifies the the build process involving submodules and allows to create custom targets.


## Clone and Build

```
git clone --recurse-submodules git@gitlab.com:luigicalligaris/openipmc-fw-pulsar2b.git
make
```

The output files files `openipmc-fw_CM7.bin`and `upgrade.hpm` are copied into the project root.

This project is built automatically by GitLab via CI script. The outputs can be found in *artifacts*.



## CI script

An example of `.gitlab-ci.yml` is provided to allow automatic build.

**NOTE**: OpenIPMC-FW submodule requires `zlib` and `libssl` to calculate CRC32 and MD5, which are used generate the `.hpm` file. These packages must be installed into the docker by the CI script.
