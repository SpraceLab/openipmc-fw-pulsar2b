
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "terminal.h"
#include "mt_printf.h"

#include "stdio.h"
#include "string.h"
#include "stdlib.h"


/*
 * External variable to define FPGA temp value
 *
 */

extern uint8_t raw_temp;



/*
 * Multitask version for the original CLI_GetIntState() provided by terminal.
 *
 * Due to the multitask approach of this project, this function must be used
 * in the callbacks to test if ESC was pressed.
 */
extern bool mt_CLI_GetIntState();



static uint8_t example_cb()
{
	mt_printf( "\r\n" );

	while( !mt_CLI_GetIntState() )
	{
		mt_printf( "This is the EXAMPLE command. Press ESC to quit.\r\n" );
		vTaskDelay( pdMS_TO_TICKS( 500 ) );
	}

	return TE_OK;
}


/*
 * Command to change FPGA sensor TEMP sensor
 */
static uint8_t change_temp()
{
  int const argc = CLI_GetArgc();

  if (argc < 1)
    return TE_ArgErr;

  raw_temp = atoi(CLI_GetArgv()[1]);

  mt_printf( "\r\n" );
  mt_printf("The sensor value was updated to: %d",raw_temp);

  return TE_OK;
}




/*
 * This functions is called during terminal initialization to add custom
 * commands to the CLI by using CLI_AddCmd functions.
 *
 * Use the CLI_AddCmd() function according terminal documentation
 */
void add_board_specific_terminal_commands( void )
{
	CLI_AddCmd( "example", example_cb, 0, TMC_None, "Run EXAMPLE command" );
	CLI_AddCmd( "sensor", change_temp, 0, TMC_None, "Change Sensor value" );
}
