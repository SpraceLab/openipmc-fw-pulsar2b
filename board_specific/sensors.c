
#include <stdio.h>
#include <stdint.h>


#include "sdr_definitions.h"
#include "sensors_templates.h"
#include "printf.h"


/*
 * Sensor reading callbacks
 *
 * These callbacks are assigned to each sensor when the sensors are created.
 * They are defined at the end of this file
 *
 * Sensors are create in the create_board_specific_sensors() hook below.
 *
 *
 */
sensor_reading_status_t sensor_reading_fpga_temp(sensor_reading_t* sensor_reading, sensor_thres_values_t* sensor_thres_values);
sensor_reading_status_t sensor_reading_air_temp(sensor_reading_t* sensor_reading, sensor_thres_values_t* sensor_thres_values);
sensor_reading_status_t sensor_reading_vcc_out(sensor_reading_t* sensor_reading, sensor_thres_values_t* sensor_thres_values);

/*
 * Sensor actions callbacks
 *
 * These callback are assigned to each sensor when it is created.
 * Each one is executed according the events generated for actual sensor reading.
 *
 */
void sensor_action_fpga_temp( ipmi_sel_entry_t * sel_entry);


/*
 * Sensor variable
 */
uint8_t raw_temp = 55;

/*
 * Hook for creating board specific sensors
 *
 * This function is called during the OpenIPMC initialization and is dedicated
 * for creating the board-specific sensors.
 */
void create_board_specific_sensors(void)
{

	analog_sensor_1_init_t sensor1;
	analog_sensor_1_init_t sensor2;
	analog_sensor_1_init_t sensor3;

	uint8_t threshold_list[6];

	// Dummy sensor for FPGA temperature.
	threshold_list[0] = 30;   // Lower Non Recoverable 30°C
	threshold_list[1] = 40;   // Lower Critical        40°C
	threshold_list[2] = 50;   // Lower Non Critical    50°C
	threshold_list[3] = 65;   // Upper Non Critical    65°C
	threshold_list[4] = 75;   // Upper Critical        75°C
	threshold_list[5] = 100;  // Upper Non Recoverable 100°C

	sensor1.sensor_type = TEMPERATURE;
	sensor1.base_unit_type = DEGREES_C;
	sensor1.m = 1;		// y = 1*x + 0  (temperature in °C is identical to it raw value)
	sensor1.b = 0;
	sensor1.b_exp = 0;
	sensor1.r_exp = 0;
	sensor1.threshold_mask_read = LOWER_NON_RECOVERABLE | LOWER_CRITICAL | LOWER_NON_CRITICAL | UPPER_NON_CRITICAL | UPPER_CRITICAL | UPPER_NON_RECOVERABLE;
	sensor1.threshold_mask_set  = LOWER_NON_RECOVERABLE | LOWER_CRITICAL | LOWER_NON_CRITICAL | UPPER_NON_CRITICAL | UPPER_CRITICAL | UPPER_NON_RECOVERABLE;
	sensor1.threshold_list = threshold_list;
	sensor1.id_string = "FPGA TEMP";
	sensor1.get_sensor_reading_func = &sensor_reading_fpga_temp;
	sensor1.sensor_action_req = &sensor_action_fpga_temp;

	create_generic_analog_sensor_1( &sensor1 );

	// Dummy sensor for Air temperature.
	threshold_list[0] = 0;    // Lower Non Recoverable  NOT USED
	threshold_list[1] = 0;    // Lower Critical         NOT USED
	threshold_list[2] = 0;    // Lower Non Critical     NOT USED
	threshold_list[3] = 100;  // Upper Non Critical     30°C  (see conversion below)
	threshold_list[4] = 120;  // Upper Critical         40°C
	threshold_list[5] = 0;    // Upper Non Recoverable  NOT USED

	sensor2.sensor_type = TEMPERATURE;
	sensor2.base_unit_type = DEGREES_C;
	sensor2.m = 5;		// y = (0.5*x - 20) = (5*x - 200)*0.1
	sensor2.b = -200;
	sensor2.b_exp = 0;
	sensor2.r_exp = -1;
	sensor2.threshold_mask_read = UPPER_NON_CRITICAL | UPPER_CRITICAL;
	sensor2.threshold_mask_set = UPPER_NON_CRITICAL | UPPER_CRITICAL;
	sensor2.threshold_list = threshold_list;
	sensor2.id_string = "AIR TEMP";
	sensor2.get_sensor_reading_func = &sensor_reading_air_temp;
	sensor2.sensor_action_req = NULL;

	create_generic_analog_sensor_1( &sensor2 );

	// Dummy sensor for Voltage.
	threshold_list[0] = 0;    // Lower Non Recoverable  NOT USED
	threshold_list[1] = 0;    // Lower Critical         NOT USED
	threshold_list[2] = 0;    // Lower Non Critical     NOT USED
	threshold_list[3] = 0;    // Upper Non Critical     NOT USED
	threshold_list[4] = 0;    // Upper Critical         NOT USED
	threshold_list[5] = 0;    // Upper Non Recoverable  NOT USED

	sensor3.sensor_type = VOLTAGE;
	sensor3.base_unit_type = VOLTS;
	sensor3.m = 1;		// y = 0.1*x = (1*x + 0)*0.1
	sensor3.b = 0;
	sensor3.b_exp = 0;
	sensor3.r_exp = -1;
	sensor3.threshold_mask_read = 0;
	sensor3.threshold_mask_set = 0;
	sensor3.threshold_list = threshold_list;
	sensor3.id_string = "12V_RAIL";
	sensor3.get_sensor_reading_func = &sensor_reading_vcc_out;
	sensor3.sensor_action_req = NULL;

	create_generic_analog_sensor_1( &sensor3 );
}


/*
 * Sensor Reading functions
 */
sensor_reading_status_t sensor_reading_fpga_temp(sensor_reading_t* sensor_reading, sensor_thres_values_t* sensor_thres_values)
{
	// Fill the raw temp field
	sensor_reading->raw_value = raw_temp;

	// Fill the threshold flag field
	sensor_reading->present_state = 0;


	if(raw_temp < sensor_thres_values->lower_non_critical_threshold)
	  sensor_reading->present_state = LOWER_NON_CRITICAL;
  if(raw_temp < sensor_thres_values->lower_critical_threshold)
    sensor_reading->present_state = LOWER_CRITICAL;
  if(raw_temp < sensor_thres_values->lower_non_recoverable_threshold)
      sensor_reading->present_state = LOWER_NON_RECOVERABLE;
	if(raw_temp > sensor_thres_values->upper_non_critical_threshold)
		sensor_reading->present_state = UPPER_NON_CRITICAL;
	if(raw_temp > sensor_thres_values->upper_critical_threshold)
		sensor_reading->present_state = UPPER_CRITICAL;
	if(raw_temp > sensor_thres_values->upper_non_recoverable_threshold)
		sensor_reading->present_state = UPPER_NON_RECOVERABLE;

	return SENSOR_READING_OK;
}

void sensor_action_fpga_temp(ipmi_sel_entry_t* ipmi_sel_entry)
{
  uint8_t event_dir  = ipmi_sel_entry->event_dir_type & SEL_EVENT_RECORDS_DIR_MASK; // to get event dir (assertion or deassertion)
  uint8_t event_code = ipmi_sel_entry->event_data_1   & EVENT_CODE_MASK;            // to get the event code.
  int reading_value = ipmi_sel_entry->event_data_2;
  int threshold = ipmi_sel_entry->event_data_3;

  if ( event_dir == DIR_ASSERTION_EVENT )
  {
    switch(event_code)
    {
      case THRESHOLD_LOWER_NON_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_LOWER_NON_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_LOWER_NON_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_LOWER_NON_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_LOWER_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_LOWER_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_LOWER_NON_RECOVERABLE_GOING_LOW:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_LOWER_NON_RECOVERABLE_GOING_LOW \n\r");
        break;
      case THRESHOLD_LOWER_NON_RECOVERABLE_GOING_HIGH:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_LOWER_NON_RECOVERABLE_GOING_HIGH \n\r");
        break;
      case THRESHOLD_UPPER_NON_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_UPPER_NON_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_UPPER_NON_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_UPPER_NON_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_UPPER_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_UPPER_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_UPPER_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_UPPER_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_UPPER_NON_RECOVERABLE_GOING_LOW:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_UPPER_NON_RECOVERABLE_GOING_LOW \n\r");
        break;
      case THRESHOLD_UPPER_NON_RECOVERABLE_GOING_HIGH:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_UPPER_NON_RECOVERABLE_GOING_HIGH \n\r");
        break;
      default:
        break;
    }
  }
  else
  {
    switch(event_code)
    {
      case THRESHOLD_LOWER_NON_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_LOWER_NON_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_LOWER_NON_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_LOWER_NON_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_LOWER_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_LOWER_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_LOWER_NON_RECOVERABLE_GOING_LOW:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_LOWER_NON_RECOVERABLE_GOING_LOW \n\r");
        break;
      case THRESHOLD_LOWER_NON_RECOVERABLE_GOING_HIGH:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_LOWER_NON_RECOVERABLE_GOING_HIGH \n\r");
        break;
      case THRESHOLD_UPPER_NON_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_UPPER_NON_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_UPPER_NON_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_UPPER_NON_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_UPPER_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_UPPER_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_UPPER_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_UPPER_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_UPPER_NON_RECOVERABLE_GOING_LOW:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_UPPER_NON_RECOVERABLE_GOING_LOW \n\r");
        break;
      case THRESHOLD_UPPER_NON_RECOVERABLE_GOING_HIGH:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_UPPER_NON_RECOVERABLE_GOING_HIGH \n\r");
        break;
      default:
        break;
    }
  }

  mt_printf("Value: %d, Threshold: %d  \n\r",reading_value,threshold);

  return;
}


sensor_reading_status_t sensor_reading_air_temp(sensor_reading_t* sensor_reading, sensor_thres_values_t* sensor_thres_values)
{

	// This sensor uses y = (0.5*x - 20) for conversion

	uint8_t raw_temp = 97; // 28.5°C

	// Fill the raw temp field
	sensor_reading->raw_value = raw_temp;

	// Fill the threshold flag field
	sensor_reading->present_state = 0;
	if(raw_temp > 100) // 30°C
		sensor_reading->present_state |= UPPER_NON_CRITICAL;
	if(raw_temp > 120) // 40°C
		sensor_reading->present_state |= UPPER_CRITICAL;

	return SENSOR_READING_OK;
}

sensor_reading_status_t sensor_reading_vcc_out(sensor_reading_t* sensor_reading, sensor_thres_values_t* sensor_thres_values)
{

	// This sensor uses y = 0.1*x for conversion
	sensor_reading->raw_value = 124; // 12.4V

	sensor_reading->present_state = 0; // No thresholds supported by this sensor

	return SENSOR_READING_OK;
}
